const express = require('express')

const app = express()

app.use(express.json())

app.get('/', (req, res) => {
    res.send({'message': "All users are coming up"})
})

const PORT = process.env.PORT || 5001
app.listen(PORT, console.log(`Server running in ${PORT}`))